Component({
  
  /**
   * 组件的属性列表
   */
  properties: { 
    show:{
      type:Boolean,
      value:false
    },
    disasters:{
      type:String,
      value:null
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    dCover:'',
    dTypes:'',
    dDiscription:'',
    dSolution:'',
  },
  /**
   * 组件的方法列表
   */
  methods: {
    popup(e) {
      console.log(this.data);
      const type = this.data.disasters;
      switch(type) {
        case 'drought':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2Fdrought.jpg",
            dTypes:'旱灾',
            dDiscription:'旱灾，是指长期无雨或少雨，使土壤水分不足、作物水分平衡遭到破坏而减产的气象灾害。',
            dSolution:'及时采摘、节水灌溉、土壤施抗旱保水剂。抗旱保墒（开展中耕锄划固土，运用行与行秸杆遮盖等新技术，降低土壤水分挥发；与此同时执行根外追肥，叶片喷洒抗旱剂、萘乙酸及叶肥，提高农作物抗旱抗高热水平）注水抗旱（注水抗旱：灵活运用水塘、水利枢纽、水沟、深水井，推行人工或机械打水抗旱。应尽早充分发挥抽水泵功效，于在下午5时后到夜里整夜通宵打水浇灌，水流量足能够推行渗灌；若水资源比较有限，可推行沟灌，再人工酌情考虑由沟中采水灌溉树杆）、雨后中耕等',
          })
          break
        case 'flood': 
        this.setData({
          dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2Fflood.jpg",
          dTypes:'洪涝',
          dDiscription:'洪涝自古以来，洪涝灾害一直是困扰人类社会发展的自然灾害。中国有文字记载的第一页就是劳动人民和洪水斗争的光辉画卷--大禹大禹治水。迄今为止，洪涝依然是对人类影响最大的灾害。',
          dSolution:'洪涝灾害的防治工作包括两个方面：一方面减少洪涝灾害发生的可能性，另一方面尽可能使已发生的洪涝灾害的损失降到最低。加强堤防建设、河道整治以及水库工程建设是避免洪涝灾害的直接措施，长期持久地推行水土保持可以从根本上减少发生洪涝的机会。切实做好洪水、天气的科学预报与滞洪区的合理规划可以减轻洪涝灾害的损失。建立防汛抢险的应急体系，是减轻灾害损失的最后措施。',
        })
          break
        case 'frozen':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2Ffrozen.jpg",
            dTypes:'冻害',
            dDiscription:'冻害的造成与降温速度、低温的强度和持续时间，低温出现前后和期间的天气状况、气温日较差等及各种气象要素之间的配合有关。在植株组织处于旺盛分裂增殖时期，即使气温短时期下降，也会受害；相反，休眠时期的植物体则抗冻性强。各发育期的抗冻能力一般依下列顺序递减：花蕾着色期→开花期→座果期    霜冻灾害的防御。',
            dSolution:'防御霜冻灾害的方法主要有3类。一是物理方法，如熏烟法；二是农业技术措施，如灌溉法、覆盖法；三是化学方法，喷施各种防霜剂、抗霜剂能有效地防御霜冻的危害。',
          })
          break
        case 'plague-of-insects':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2Fplague-of-insects.jpg",
            dTypes:'虫灾',
            dDiscription:'虫灾属于有害生物繁殖过量型灾害，指由于某种昆虫的发生量过大，吞食大量农作物，从而造成饥馑的自然灾害。',
            dSolution:'常见生物防治病虫害方法有：蓖麻叶防治葱、韭菜、大蒜、萝卜、白菜的地蛆、菜青虫、食叶甲等；桃叶液可防治棉蚜、玉米螟、稻苞虫。也可用桃叶加水煮后过滤，取原液喷洒，能防止稻叶蝉、稻飞虱；桑叶合剂防治红蜘蛛；马尾松液防治稻叶蝉、稻飞虱； 松叶抑制马铃薯发芽；臭椿叶浸出液直接喷雾防治蔬菜蚜虫、菜青虫等害虫；或防治小麦锈病；草木灰可防治蚜虫；花椒能防治蚜虫、叶蝉、白粉虱、黏虫、蚧壳虫等',
          })
          break
        case 'wind-damage':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2Fwinddamage.jpg",
            dTypes:'风害',
            dDiscription:'风害，是一种农业气象灾害，是指大风对农业生产造成的直接和间接危害。主要指造成土壤风蚀沙化、对作物的机械损伤和生理危害，同时也影响农事活动和破坏农业生产设施；',
            dSolution:'防御冬春季风害和沙尘暴的主要措施有：大力加强植树造林，营造绿带，建设农田林网，削弱风害影响，保护农田和农业基础设施。开源节流，涵养水源，提高自然降水的利用率，种植抗寒、抗旱的作物。选择抗风树种，在种植设计时，风口、风道处选择抗风性强的树种，如垂柳、乌桕等，选择根深、矮干、枝叶稀疏坚韧的树木品种。不要选择生长迅速而枝叶茂密及一些易受虫害的树种',
          })
          break
        case 'landslide':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2FNiShiLiu.jpg",
            dTypes:'滑坡',
            dDiscription:'滑坡、泥石流等地质灾害：这些灾害可能直接摧毁农田和作物，造成严重的农业损失甚至人员伤亡。',
            dSolution:'防治泥石流的措施包括禁止在流域内进行滥砍滥伐，保护植被;严禁在坡度大于25°的地区进行垦荒种地;防治泥石流的农业措施包括退耕还林、等高耕作、滑坡体上水田变旱地、开发利用泥石流堆积扇等。',
          })
          break
        case 'land-salinization':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2FYanJianHua.jpg",
            dTypes:'盐碱',
            dDiscription:'土壤盐渍化是指土壤底层或地下水的盐分随毛管水上升到地表，水分蒸发后，使盐分积累在表层土壤中的过程。是指易溶性盐分在土壤表层积累的现象或过程，也称盐碱化。',
            dSolution:'改良耕作方式，如深耕、深松等，可以打破土壤板结，增加土壤通透性，有利于盐分淋洗。施客土，即将非盐碱化的土壤与盐碱化土壤混合，降低土壤盐分含量。合理施肥，避免过量使用化肥，尤其是氮肥，以免增加土壤盐分。播种耐盐碱的作物品种，并采用轮作、间作、套种等种植方式，提高土地利用率，减轻土壤盐碱化。种植耐盐碱的植物，如碱蓬、盐角草等，它们能够吸收土壤中的盐分，降低土壤盐分含量。',
          })
          break
          case 'mouse':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2FshuHuan.jpg",
            dTypes:'鼠患',
            dDiscription:'老鼠是一种贪吃的动物，每只老鼠每天要吃掉相当于它体重的1/5的食物据估计，每年生产的粮食约有5%被老鼠夺去，全世界每年被损耗的粮食有5000万吨，损失上亿美元',
            dSolution:'解决方法有：1.鼓励天敌如猫、猫头鹰等在农田周围活动；配置毒饵防鼠；2.选择无变质的小麦、大米或玉米作饵料，按一定比例添加化学药物制成毒饵，然后放置在老鼠经常出没的地方。3.根据实际情况及用药历史，注意轮换使用敌鼠钠盐、杀鼠醚水剂和溴敌隆等灭鼠药，防止害鼠产生抗药性。（严禁使用国家明令禁止的剧毒急性鼠药及市场上无“三证”的鼠药）',
          })
          break
          case 'disease':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2FbingDu.jpg",
            dTypes:'病害',
            dDiscription:'植物病害的病状主要分为变色、坏死、腐烂、萎蔫、畸形五大类型。',
            dSolution:'植物病害防治的原则是：1.消灭病原物或抑制其发生与蔓延；2.提高寄主植物的抗病能力；3.控制或改造环境条件，使之有利于寄主植物而不利于病原物，抑制病害的发生和发展。4.一般着重于植物群体的预防，因地因时根据作物病害的发生、发展规律，采取综合防治措施。（不建议大量焚烧受感染作物，有风险）4.每项措施要能充分发挥农业生态体系中的有利因素，避免不利因素，避免公害和人畜中毒。防治方法有植物检疫、抗病育种、农业防治、化学防治、物理和机械防治和生物防治等。',
          })
          break
          case 'acid-rain':
          this.setData({
            dCover:"https://daolunpxy-1326125744.cos.ap-beijing.myqcloud.com/images%2FsuanYu.jpg",
            dTypes:'酸雨',
            dDiscription:'酸雨是指pH小于5.6的雨雪或其他形式的降水，主要是人为的向大气中排放大量酸性物质所造成的。',
            dSolution:'常见防治措施有：1.在农田中种植绿肥，如紫云英、苜蓿等，这些植物在生长过程中可以吸收土壤中的酸性物质，降低土壤的酸化程度。同时，施用有机肥也能提高土壤的缓冲能力，缓解土壤酸化过程。2.施用石灰等中和剂：在已经酸化的土壤中，可以酌情施用生石灰等中和剂，以中和土壤中的酸性物质，降低土壤的酸度。3.采用设施农业技术：利用大棚等设施农业技术，通过塑料薄膜的隔离作用，减轻酸雨对农作物的直接危害。这种方法可以在酸雨频发地区有效地保护农作物，确保农业生产的稳定。',
          })
          break
      }

    },
  }
})
