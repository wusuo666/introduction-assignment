Page({  
  // 页面数据  
  data: {  
    disasterPopupShow:false,
    currentDisasters:"",
  },  
  // 生命周期函数  
  onLoad: function (options) {  
  
    // ...  
  },  
    
  onShow: function (options) {
    
  },

  popup(e) {
    const type = e.currentTarget.dataset.type;
    this.setData({
      disasterPopupShow:true,
      currentDisasters:type
    })
    console.log(this.data.currentDisasters)
  },


  // 返回主页的函数  
  backToHomePage: function() {  
    wx.redirectTo({  
      url: '/pages/index/index' // 假设你的主页路径是 /pages/index/index  
    });  
  }  
});