Page({  
  // 页面数据  
  data: {  
    playerHealth: 100, 
    playerGrow: 0, 
    flag: 0,
    isplayerHealthGreaterThan25: false,
    isplayerHealthGreaterThan50: false,
    isplayerHealthGreaterThan75: false,
    isplayerHealthGreaterThan100: false,
    isplayerGrowGreaterThan25: false,
    isplayerGrowGreaterThan50: false,
    isplayerGrowGreaterThan75: false,
    isplayerGrowGreaterThan100: false,

    waterX: 100 * (wx.getSystemInfoSync().windowWidth / 750)  ,
    waterY: (35 / 100) * wx.getSystemInfoSync().windowHeight,

    WchaX: 0,
    WchaY: 0,
    Wtouch: false,
    WposX:1,
    WposY:86,

    FchaX: 0,
    FchaY: 0,
    Ftouch: false, 
    FposX:350, 
    FposY:85, 

    showDialogS: false,
    showDialogF: false,
    showQuestionS:false,
    showQuestionF:false,
    showHelp: false,

    showArray : [false,false,false,false,false,false,false,false,false,false,false,false,false,false],
  },  
    
  // 生命周期函数  
  onLoad: function (options) { 
    this.judgeGrow();
    this.animationDataW = wx.createAnimation({
      duration: 1000
    });
    this.animationDataF = wx.createAnimation({
      duration: 1000
    });
    
    // ...   
  },  
    
  onShow: function (options) {

  },

  judgeGrow:function()
  {
    var app = getApp();  
    if (this.data.playerHealth < 25) {  
      this.setData({  
        isplayerHealthGreaterThan25: true  
      });  
    } else {  
      this.setData({  
        isplayerHealthGreaterThan25: false  
      });  
    }  

    if (this.data.playerHealth < 50) {  
      this.setData({  
        isplayerHealthGreaterThan50: true  
      });  
    } else {  
      this.setData({  
        isplayerHealthGreaterThan50: false  
      });  
    }  

    if (this.data.playerHealth < 75) {  
      this.setData({  
        isplayerHealthGreaterThan75: true  
      });  
    } else {  
      this.setData({  
        isplayerHealthGreaterThan75: false  
      });  
    }  

    if (this.data.playerHealth < 100) {  
      this.setData({  
        isplayerHealthGreaterThan100: true  
      });  
    } else {  
      this.setData({  
        isplayerHealthGreaterThan100: false  
      });  
    }  

    if (this.data.playerGrow < 25) {  
      this.setData({  
        isplayerGrowGreaterThan25: true  
      });  
    } else {  
      this.setData({  
        isplayerGrowGreaterThan25: false  
      });  
    }  

    if (this.data.playerGrow < 50) {  
      this.setData({  
        isplayerGrowGreaterThan50: true  
      });  
    } else {  
      this.setData({  
        isplayerGrowGreaterThan50: false  
      });  
    }  

    if (this.data.playerGrow < 75) {  
      this.setData({  
        isplayerGrowGreaterThan75: true  
      });  
    } else {  
      this.setData({  
        isplayerGrowGreaterThan75: false  
      });  
    }  

    if (this.data.playerGrow < 100) {  
      this.setData({  
        isplayerGrowGreaterThan100: true  
      });  
    } else {  
      this.setData({  
        isplayerGrowGreaterThan100: false  
      });  
    }  

    if(this.data.playerGrow>=100){
      this.setData({
        showDialogS:true,
        playerGrow:100
      })
    }

    if(this.data.playerHealth<=0){
      this.setData({
        showDialogF:true,
        playerHealth:0
      })
    }

    if(this.data.flag>=15&&this.data.playerGrow<100){
      let length = this.data.showArray.length;  
      // 使用Math.random()生成一个0到length-1之间的随机整数  
      let randomIndex = Math.floor(Math.random() * length);  
      // 返回随机索引对应的数组元素  

      let tempArray = this.data.showArray;  
      tempArray[randomIndex] = true;  
      this.setData({
        showArray: tempArray,
        flag:this.data.flag-15
      })
    }
  },
  //洒水壶拖动的逻辑
  touchStartW: function (e) {
    console.log("== touchStart ==");// 拖动开始
    // e.touches[0] 内容就是触摸点的坐标值
    var WtranX = (e.touches[0].pageX/ wx.getSystemInfoSync().windowWidth)*750-this.data.WposX;
    var WtranY = (e.touches[0].pageY/ wx.getSystemInfoSync().windowHeight)*100-this.data.WposY;
    console.log("start tranX: " + WtranX);
    console.log("start tranY: " + WtranY);
    // 存储chaX和chaY 并设置 touch: true
    this.setData({
        Wtouch: true, 
        WchaX:WtranX,
        WchaY:WtranY
    });
  },
  // 触摸移动
  touchMoveW: function (e) {
    if (!this.data.Wtouch) return;
    // e.touches[0] 内容就是触摸点的坐标值
    var new_WposX = (e.touches[0].pageX/ wx.getSystemInfoSync().windowWidth)*750-this.data.WchaX;
    var new_WposY = (e.touches[0].pageY/ wx.getSystemInfoSync().windowHeight)*100-this.data.WchaY;
    // console.log(" move new_WposX: " + new_WposX);
    // console.log(" move nwe_WposY: " + new_WposY);
    this.setData({
        WposX: new_WposX,
        WposY: new_WposY
    });
  },
  // 触摸结束
  touchEndW: function (e) {
    console.log("== touchEnd ==")
    if (!this.data.Wtouch) return;
    if(
      (this.data.WposX - 300) < 280 &&
      (this.data.WposX - 300) > -280 &&
      (this.data.WposY - 45) < 17 &&
      (this.data.WposY - 45) > -17){
      this.setData({
        playerGrow:this.data.playerGrow+6,
        flag:this.data.flag+6
      });
      this.animationDataW.translateX(350 * (wx.getSystemInfoSync().windowWidth / 750)).opacity(1).step()      
      this.animationDataW.translateX(0).opacity(0).step()    
      this.setData({
        animationDataW:this.animationDataW.export(),
      })
    }
    else console.log(1)
    this.setData({
        Wtouch:false,
        WchaX:0,
        WchaY:0,
        WposX:1,
        WposY:86,
    });
    this.judgeGrow();
  },
  //肥料拖动
  touchStartF: function (e) {
    console.log("== touchStart ==");// 拖动开始
    // e.touches[0] 内容就是触摸点的坐标值
    var FtranX = (e.touches[0].pageX/ wx.getSystemInfoSync().windowWidth)*750-this.data.FposX;
    var FtranY = (e.touches[0].pageY/ wx.getSystemInfoSync().windowHeight)*100-this.data.FposY;
    console.log("start FtranX: " + FtranX);
    console.log("start FtranY: " + FtranY);
    // 存储chaX和chaY 并设置 touch: true
    this.setData({
        Ftouch: true, 
        FchaX:FtranX,
        FchaY:FtranY
    });
  },
  // 触摸移动
  touchMoveF: function (e) {
    if (!this.data.Ftouch) return;
    // e.touches[0] 内容就是触摸点的坐标值
    var new_FposX = (e.touches[0].pageX/ wx.getSystemInfoSync().windowWidth)*750-this.data.FchaX;
    var new_FposY = (e.touches[0].pageY/ wx.getSystemInfoSync().windowHeight)*100-this.data.FchaY;
    // console.log(" move new_WposX: " + new_FposX);
    // console.log(" move nwe_WposY: " + new_FposY);
    this.setData({
        FposX: new_FposX,
        FposY: new_FposY
    });
  },
  // 触摸结束
  touchEndF: function (e) {
    console.log("== touchEnd ==")
    if (!this.data.Ftouch) return;
    if(
      (this.data.FposX - 300) < 280 &&
      (this.data.FposX - 300) > -280 &&
      (this.data.FposY - 45) < 17 &&
      (this.data.FposY - 45) > -17){
      this.setData({
        playerGrow:this.data.playerGrow+8,
        flag:this.data.flag+8
      });
      
      this.animationDataF.translateX(350 * (wx.getSystemInfoSync().windowWidth / 750)).opacity(1).step()      
      this.animationDataF.translateX(0).opacity(0).step()    
      this.setData({
        animationDataF:this.animationDataF.export(),
      })
    }
    
    this.setData({
        Ftouch:false,
        FchaX:0,
        FchaY:0,
        FposX:350, 
        FposY:85, 
    });
    this.judgeGrow();
    
  },

  toggleDialogS() {
    this.setData({
      showDialogS: !this.data.showDialogS
    })
  },
  toggleDialogF() {
    this.setData({
      showDialogF: !this.data.showDialogF
    })
  },


  // 返回主页的函数  
  backToHomePage: function() {  
    wx.redirectTo({  
      url: '/pages/index/index'  
    });  
  },
  getHelp: function() {
    this.setData({
      showHelp: true
    })
    console.log(this.data.showHelp)
  },
  gameagainS: function() {
    this.setData({
      playerGrow:0,
      playerHealth:100,
      flag:0,
      showDialogS: !this.data.showDialogS
    })
    this.judgeGrow()
  },
  gameagainF: function() {
    this.setData({
      playerGrow:0,
      playerHealth:100,
      flag:0,
      showDialogF: !this.data.showDialogF
    })
    this.judgeGrow()
  },
  questionbackS: function() {
    this.setData({
      showQuestionS: false
    })
  },
  questionbackF: function() {
    this.setData({
      showQuestionF: false
    })
  },
  //帮助页面返回
  helpBack: function() {
    this.setData({
      showHelp: false
    })
  },
  //突发事件的失败与成功
  emergencyFail1: function(){
    let tempArray = this.data.showArray;  
    tempArray[0] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess1() {
    let tempArray = this.data.showArray;  
    tempArray[0] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail2: function(){
    let tempArray = this.data.showArray;  
    tempArray[1] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess2() {
    let tempArray = this.data.showArray;  
    tempArray[1] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail3: function(){
    let tempArray = this.data.showArray;  
    tempArray[2] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess3() {
    let tempArray = this.data.showArray;  
    tempArray[2] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail4: function(){
    let tempArray = this.data.showArray;  
    tempArray[3] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess4() {
    let tempArray = this.data.showArray;  
    tempArray[3] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail5: function(){
    let tempArray = this.data.showArray;  
    tempArray[4] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess5() {
    let tempArray = this.data.showArray;  
    tempArray[4] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail6: function(){
    let tempArray = this.data.showArray;  
    tempArray[5] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess6() {
    let tempArray = this.data.showArray;  
    tempArray[5] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail7: function(){
    let tempArray = this.data.showArray;  
    tempArray[6] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess7() {
    let tempArray = this.data.showArray;  
    tempArray[6] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail8: function(){
    let tempArray = this.data.showArray;  
    tempArray[7] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess8() {
    let tempArray = this.data.showArray;  
    tempArray[7] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail9: function(){
    let tempArray = this.data.showArray;  
    tempArray[8] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess9() {
    let tempArray = this.data.showArray;  
    tempArray[8] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail10: function(){
    let tempArray = this.data.showArray;  
    tempArray[9] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess10() {
    let tempArray = this.data.showArray;  
    tempArray[9] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail11: function(){
    let tempArray = this.data.showArray;  
    tempArray[10] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess11() {
    let tempArray = this.data.showArray;  
    tempArray[10] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail12: function(){
    let tempArray = this.data.showArray;  
    tempArray[11] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess12() {
    let tempArray = this.data.showArray;  
    tempArray[11] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail13: function(){
    let tempArray = this.data.showArray;  
    tempArray[12] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess13() {
    let tempArray = this.data.showArray;  
    tempArray[12] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
  emergencyFail14: function(){
    let tempArray = this.data.showArray;  
    tempArray[13] = false;  
    this.setData({
      playerHealth:this.data.playerHealth-25,
      showArray:tempArray,
      showQuestionF: true
    })
    this.judgeGrow()
  },
  emergencysuccess14() {
    let tempArray = this.data.showArray;  
    tempArray[13] = false;  
    this.setData({
      showArray:tempArray,
      showQuestionS: true
    })
  },
});