Page({
  data: {
    showKnow: false,
    attentionAnim: '',
  },
  onReady: function () {
    var attentionAnim = wx.createAnimation({
      duration: 2000,
      timingFunction: 'ease',
      delay: 0
    })
    //设置循环动画
    this.attentionAnim = attentionAnim
    var next = true;
    setInterval(function () {
      if (next) {
        //根据需求实现相应的动画
        this.attentionAnim.scale(0.8,0.8).rotate(8).step()
        next = !next;
      } else {
        this.attentionAnim.scale(1,1).rotate(-8).step()
        next = !next;
      }
      this.setData({
        //导出动画到指定控件animation属性
        attentionAnim: attentionAnim.export()
      })
    }.bind(this), 150)
  },
  button1Clicked: function() {  
    // 按钮点击后的处理逻辑  
    console.log('按钮1被点击了！');  
    wx.redirectTo({  
      url: '/pages/game/game'  
    })
  },
  button2Clicked: function() {  
    // 按钮点击后的处理逻辑  
    console.log('按钮2被点击了！');  
    wx.redirectTo({  
      url: '/pages/handbook/handbook'  
    })
  },
  button3Clicked: function() {  
    // 按钮点击后的处理逻辑  
    console.log('按钮3被点击了！');  
    this.setData({
      showKnow: true
    })
  },
  knowBack: function() {
    this.setData({
      showKnow: false
    })
  },
  
});
